FROM python:3.8

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /

RUN apt-get update \
    && apt-get install -y gcc python3-dev musl-dev  libffi-dev netcat
RUN apt-get install -y fping
RUN apt-get install -y nmap
RUN apt-get install -y hping3
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tshark
RUN apt-get install -y ettercap-text-only
RUN apt-get install -y ettercap-graphical
RUN apt-get install -y tcpxtract
RUN apt-get install -y iptables
RUN apt-get install -y dsniff
RUN apt-get install -y build-essential
RUN apt-get install -y hydra
RUN apt-get install -y openssl
RUN apt-get install -y libssl-dev
RUN apt install -y python3-pip

COPY . .
RUN pip install -r requirements.txt

# RUN python3 manage.py makemigrations
# RUN python3 manage.py migrate
# RUN python manage.py collectstatic --noinput
# RUN python3 manage.py createsuperuser --noinput --username admin123 --email admin@gmail.com --password Abc@12345
# RUN python3 manage.py loaddata initial_data.json
# RUN python3 manage.py runserver 0.0.0.0:9090
EXPOSE 9090