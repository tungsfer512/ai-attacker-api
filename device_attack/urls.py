
from device_attack import views
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    
    path('attack/dos/hping3/', csrf_exempt(views.DoS_HPing3.as_view())),
    path('attack/dos/goldeneye/', csrf_exempt(views.DoS_GoldenEye.as_view())),
    
    path('attack/access/ettercap/', csrf_exempt(views.Access_Ettercap.as_view())),
    path('attack/access/sqlmap/', csrf_exempt(views.Access_SQLMap.as_view())),
    path('attack/access/hydra/', csrf_exempt(views.Access_Hydra.as_view())),
    path('attack/access/backdoor/', csrf_exempt(views.Access_Backdoor.as_view())),
    path('attack/access/backdoor/upload/', csrf_exempt(views.Access_Backdoor_Upload.as_view())),
    path('attack/access/backdoor/<str:filename>/', csrf_exempt(views.Access_Backdoor_Download.as_view())),
    path('attack/access/backdoor/<str:filename>/delete/', csrf_exempt(views.Access_Backdoor_Delete.as_view())),
    
    # path('attack/xss/dvpwa/', csrf_exempt(views.Reconnaissance_Nmap.as_view())),
    
    path('attack/reconnaissance/fping/', csrf_exempt(views.Reconnaissance_Fping.as_view())),
    path('attack/reconnaissance/pyshark/', csrf_exempt(views.Reconnaissance_PyShark.as_view())),
    path('attack/reconnaissance/nmap/', csrf_exempt(views.Reconnaissance_Nmap.as_view())),
    
    # path('attack/mail-phising/', csrf_exempt(views.Reconnaissance_Nmap.as_view())),
    
    
]
