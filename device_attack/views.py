from wsgiref.util import FileWrapper
from rest_framework import generics, status
import subprocess
import re
from django.http import JsonResponse, HttpResponse, FileResponse
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from device_attack.tools.DoS.Hping3 import flood as dos_hping3
from device_attack.tools.Reconnaissance.pyshark_1 import pyshark_1
from device_attack.tools.Access.EterCap import ettercap
import os
from pathlib import Path
import environ
from rest_framework.views import APIView
from rest_framework import permissions
from datetime import datetime
from rest_framework.parsers import MultiPartParser
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework import status
import mimetypes
import time

# Build paths inside the project like this: BASE_DIR / 'subdir'.

env = environ.Env(
    ROOT_PASSWORD=(str, '')
)

BASE_DIR = Path(__file__).resolve().parent.parent
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))


PRIVILEGES = f"echo {env('ROOT_PASSWORD')} | sudo -S "
# PRIVILEGES = ""


class CustomPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'limit'
    max_page_size = 1000


class Reconnaissance_Nmap(generics.CreateAPIView):

    pagination_class = CustomPagination

    def create(self, req, *args, **kwargs):
        print("--------------------------")
        print(req.data)
        print("--------------------------")
        data = []
        for x in range(int(req.data['solan'])):
            cmd1 = ['echo', f'{env("ROOT_PASSWORD")}']
            cmd = ['nmap', req.data['ip']]
            if "options" in req.data.keys():
                for i in req.data['options'].split(" "):
                    cmd.append(i)

            result1 = subprocess.Popen(cmd1, stdout=subprocess.PIPE)
            result = subprocess.Popen(
                ['sudo', '-S'] + cmd, stdin=result1.stdout, stdout=subprocess.PIPE)
            data_out = result.stdout.readlines()
            print("==============================")
            print(data_out)
            print(type(data_out))
            print("==============================")
            res = []
            ok = False
            for line in data_out:
                line = line[:-1].decode()
                print("*** ", line)
                if ("PORT" in line) and ("STATE" in line) and ("SERVICE" in line):
                    ok = True
                    continue
                if '/' not in line:
                    ok = False
                if ok == True:
                    res.append(line)
            if "options" in req.data.keys():
                if "-d" in req.data['options']:
                    res = res[:-1]
            print(res)

            res_a = []
            for i in range(len(res)):
                temp = re.split('\s+', res[i])
                port = temp[0].split('/')
                temp_d = {
                    'id': i + 1,
                    'port': port[0],
                    'protocol': port[1],
                    'state': temp[1],
                    'service': temp[2],
                    'reason': temp[3] if len(temp) >= 4 else "",
                }
                res_a.append(temp_d)
            time.sleep(int(req.data['thoigian']))
            data.append({
                "id": x + 1,
                'data': res_a,
                'total': len(res_a)
            })
            print(data)
        return JsonResponse({
            'data': data,
        }, status=status.HTTP_200_OK)


class Reconnaissance_Fping(generics.CreateAPIView):

    pagination_class = CustomPagination

    def create(self, req, *args, **kwargs):
        ips = req.data["ips"].strip().split(" ")
        options = ""
        if "options" in req.data.keys():
            options = req.data["options"].strip()

        result = []
        cmd = ['fping', options]
        for i in range(len(ips)):
            cmd.append(ips[i])

        temp_res = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        temp_res_str, _ = temp_res.communicate()
        print("-----------------")
        aaa = temp_res_str.decode('utf-8')
        # print(str(aaa))
        aaa = temp_res_str.decode('utf-8').splitlines()
        for i in range(len(aaa)):
            print(re.split("\s", aaa[i]))
            temp = re.split("\s", aaa[i])
            print("-----------------")
            result.append({
                "id": i + 1,
                "host": temp[0],
                "status": temp[2]
            })

        page = int(req.query_params.get('page', 1))
        limit = int(req.query_params.get('limit', 10))
        data = result
        total = len(data)
        start = (page - 1) * limit
        end = min(start + limit, total)
        print(start, end, limit)
        data = sorted(data, key=lambda x: x['id'])

        return JsonResponse({
            # 'data': data[start:end],
            'data': data,
            'total': total
        }, status=status.HTTP_200_OK)


class DoS_HPing3(generics.CreateAPIView):

    pagination_class = CustomPagination

    def create(self, req, *args, **kwargs):
        print("--------------------------")
        print(req.data)
        print("--------------------------")

        result = dos_hping3.check(req.data['ip'], req.data['options'], int(req.data['solan']), int(req.data['thoigian']))

        data = result
        print(data)

        return JsonResponse({
            # 'data': data[start:end],
            'data': data,
            # 'total': total
        }, status=status.HTTP_200_OK)


class Access_SQLMap(generics.CreateAPIView):
    def create(self, req, *args, **kwargs):
        print("-----------")
        print(req.data)

        cmd = ['python3', './device_attack/tools/Access/sqlmap/sqlmap.py',
               req.data['targetType'], req.data['targetDestination']]
        for i in req.data['options'].split(" "):
            cmd.append(i)

        data, err = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()

        res = []

        for i in data.splitlines():
            res.append(i.decode('utf-8'))

        return JsonResponse({
            'data': res
        }, status=status.HTTP_200_OK)

        super().retrieve(req, *args, **kwargs)


class Access_Hydra(generics.CreateAPIView):
    def create(self, req, *args, **kwargs):
        print("-----------")
        print(req.data)
        print(req.data['ip'])
        print(type(req.data['ip']))
        print(req.data['options'])
        print(type(req.data['options']))
        print(req.data['loaitancong'])
        print(type(req.data['loaitancong']))
        print(req.data['cuongdo'])
        print(type(req.data['cuongdo']))
        print(req.data['solan'])
        print(type(req.data['solan']))
        print(req.data['thoigian'])
        print(type(req.data['thoigian']))

        print(req.FILES)

        file_username = req.FILES.get('username')
        file_password = req.FILES.get('password')
        print(file_username)
        print(file_password)

        cmd = ['hydra']

        if req.data['cuongdo'] == 'theochuky':
            cmd.append("-w")
            cmd.append("16")

        if ((file_password == None) or (file_username == None)) and (("-l" not in req.data['options']) or ("-p" not in req.data['options'])):
            return Response("Vui long tai len file username va password hoac them vao options voi cu phap: -l <username> -p <password>", status=status.HTTP_400_BAD_REQUEST)

        if file_password != None and file_username != None:
            if (("-l" in req.data['options']) or ("-p" in req.data['options'])):
                return Response("Vui long chi tai len file username va password hoac them vao options voi cu phap: -l <username> -p <password>, khong the cung luc voi nhau", status=status.HTTP_400_BAD_REQUEST)
            filename = str(int(round(datetime.now().timestamp()))
                           ) + "_usernames.txt"
            dest_username = open(
                f"./media/Hydra/txt/{filename}", "w")
            for chunk in file_username.chunks():
                dest_username.write(chunk.decode("utf-8"))
                # print(str(chunk))
            dest_username.close()
            cmd.append("-L")
            cmd.append(f"media/Hydra/txt/{filename}")
            filename = str(int(round(datetime.now().timestamp()))
                           ) + "_passwords.txt"
            dest_password = open(
                f"./media/Hydra/txt/{filename}", "w")
            for chunk in file_password.chunks():
                dest_password.write(chunk.decode("utf-8"))
            dest_password.close()
            cmd.append("-P")
            cmd.append(f"media/Hydra/txt/{filename}")

        cmd.append(req.data['ip'])
        cmd.append(req.data['loaitancong'])
        if req.data['loaitancong'] == 'http-post-form':
            cmd.append(
                f'{req.data["pathlogin"]}:username=^USER^&password=^PASS^:F=incorrect')

        for i in req.data['options'].split(" "):
            cmd.append(i)
        res = []
        for i in range(int(req.data['solan'])):
            print(cmd)
            data, err = subprocess.Popen(
                cmd, stdout=subprocess.PIPE).communicate()

            res.append("----------------------------------------")
            res.append("Tan cong lan thu " + str(i + 1))
            res.append("----------------------------------------")

            for line in data.splitlines():
                res.append(line.decode('utf-8'))
            if i < int(req.data['solan']) - 1:
                time.sleep(int(req.data['thoigian']))

        # print(data)
        # res = []
        return JsonResponse({
            'data': res
        }, status=status.HTTP_200_OK)

class Access_Ettercap(generics.ListCreateAPIView):
    def list(self, req, *args, **kwargs):
        os.system(f'{PRIVILEGES}ettercap -G')
        return JsonResponse({
            'data': "ok"
        }, status=status.HTTP_200_OK)

    def create(self, req, *args, **kwargs):
        data = req.data
        
        iface = data.get("iface", "default")
        method = data.get("method", "ARP")
        target1 = data.get("target1", None)
        target2 = data.get("target2", None)
        times = data.get("times", 1)
        
        data = ettercap.mitm(iface=iface, method=method, target1=target1, target2=target2, times=times)
        
        return JsonResponse({
            'data': data
        }, status=status.HTTP_200_OK)


class Reconnaissance_PyShark(generics.CreateAPIView):

    def create(self, req, *args, **kwargs):

        packetCount = 100
        timeOut = None
        protocolFilters = None

        infs = req.data['infs']
        if 'packetCount' in req.data.keys():
            packetCount = req.data['packetCount']
        if 'timeOut' in req.data.keys():
            timeOut = req.data['timeOut']
        if 'protocolFilters' in req.data.keys():
            protocolFilters = req.data['protocolFilters']

        print(req.data)

        data = pyshark_1.LiveCap(infs=infs, packet_count=packetCount,
                                 time_out=timeOut, protocol_filters=protocolFilters)

        return JsonResponse({
            'data': data
        }, status=status.HTTP_200_OK)


class DoS_GoldenEye(generics.CreateAPIView):
    def create(self, req, *args, **kwargs):
        cmd = f"python3 ./device_attack/tools/DDoS/GoldenEye/goldeneye.py {req.data.get('url', '')} -t 1"
        solan = 1
        thoigian = 0
        print(req.data)
        if req.data.get("workers", None) != None and req.data.get("workers", None) != "":
            cmd += f" -w {req.data.get('workers', None)}"
        if req.data.get("sockets", None) != None and req.data.get("sockets", None) != "":
            cmd += f" -s {req.data.get('sockets', None)}"
        if req.data.get("method", None) != None and req.data.get("method", None) != "":
            cmd += f" -m {req.data.get('method', None)}"
        if req.data.get("thoigian", None) != None and req.data.get("thoigian", None) != "":
            thoigian = int(req.data.get('thoigian', None))
        if req.data.get("solan", None) != None and req.data.get("solan", None) != "":
            solan = int(req.data.get("solan", None))
        print(cmd)
        for i in range(solan):
            print("bat dau golden eye lan thu", i)
            os.system(cmd)
            if i < solan - 1:
                time.sleep(thoigian)
            print("ket thuc golden eye lan thu", i)
        # os.system('gnome-terminal')
        return JsonResponse({
            'data': 'ok'
        }, status=status.HTTP_200_OK)


class Access_Backdoor(generics.ListCreateAPIView):
    parser_classes = (MultiPartParser,)
    permission_classes = [permissions.IsAuthenticated]

    def list(self, request, *args, **kwargs):
        files = os.listdir("./media/backdoor")
        data = []
        for i in range(len(files)):
            te = {
                "id": i + 1,
                "filename": files[i],
                "name": "_".join(files[i].split("_")[1:]).upper()
            }
            data.append(te)
        print(files)
        return JsonResponse({
            "data": data,
            "count": len(data)
        }, status=status.HTTP_200_OK)


class Access_Backdoor_Upload(APIView):
    parser_classes = (MultiPartParser,)
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(operation_description='Upload file...',
                         manual_parameters=[openapi.Parameter(
                             name="file",
                             in_=openapi.IN_FORM,
                             type=openapi.TYPE_FILE,
                             required=True,
                             description="Backdoor exe file"
                         )],)
    def post(self, request):
        # file = request.data['file']
        # print(request.data)
        files = request.FILES.getlist('file')
        if len(files) == 0:
            return HttpResponse("Upload fail", status=status.HTTP_400_BAD_REQUEST)
        for file in files:
            filename = str(int(round(datetime.now().timestamp()))
                           ) + "_" + file.name.replace(" ", "_")
            dest = open(
                f"./media/backdoor/{filename}", "w+")
            for chunk in file.chunks():
                dest.write(chunk.decode("utf-8"))
                # print(str(chunk))
            dest.close()
        return HttpResponse("Upload successfully", status=status.HTTP_200_OK)


class Access_Backdoor_Download(generics.RetrieveAPIView):
    parser_classes = (MultiPartParser,)
    permission_classes = [permissions.IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        filename = kwargs.get('filename')
        file = open(f"./media/backdoor/{filename}", "rb")
        mime_type, _ = mimetypes.guess_type(
            f"./media/backdoor/{filename}")
        response = HttpResponse(file, content_type=mime_type)
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        return response


class Access_Backdoor_Delete(generics.DestroyAPIView):
    parser_classes = (MultiPartParser,)
    permission_classes = [permissions.IsAuthenticated]

    def destroy(self, request, *args, **kwargs):
        filename = kwargs.get('filename')
        os.remove(f"./media/backdoor/{filename}")
        return HttpResponse("Delete file successfully", status=status.HTTP_200_OK)
