import asyncio
import os
from pathlib import Path
import environ

# Build paths inside the project like this: BASE_DIR / 'subdir'.

env = environ.Env(
    ROOT_PASSWORD=(str, '')
)

BASE_DIR = Path(__file__).resolve().parent.parent
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))


PRIVILEGES = f"echo {env('ROOT_PASSWORD')} | sudo -S "
# PRIVILEGES = ""


async def _read_stream(stream, cb):
    while True:
        line = await stream.readline()
        if line:
            cb(line)
        else:
            break


async def _stream_subprocess(cmd, stdout_cb, stderr_cb):
    process = await asyncio.create_subprocess_exec(*cmd,
                                                   stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)

    await asyncio.wait([
        _read_stream(process.stdout, stdout_cb),
        _read_stream(process.stderr, stderr_cb)
    ])
    return await process.wait()


def execute(cmd, stdout_cb, stderr_cb):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    rc = loop.run_until_complete(
        _stream_subprocess(
            cmd,
            stdout_cb,
            stderr_cb,
        ))
    loop.close()
    return rc


def _run_(cmd):

    if 'sudo' in cmd:
        cmd = cmd.replace('sudo', PRIVILEGES)

    return execute(
        ["bash", "-c", f"{cmd}"],
        lambda x: (x.decode('utf-8').replace('\n', "")),
        lambda x: (x.decode('utf-8').replace('\n', "")),
    )


if __name__ == '__main__':
    _run_("sudo apt update")
