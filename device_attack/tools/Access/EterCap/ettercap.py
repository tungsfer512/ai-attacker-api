import sys, os, subprocess, time
from pathlib import Path
import environ

from datetime import datetime

# Build paths inside the project like this: BASE_DIR / 'subdir'.

env = environ.Env(
    ROOT_PASSWORD=(str, '')
)

BASE_DIR = Path(__file__).resolve().parent.parent
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))


PRIVILEGES = f"echo {env('ROOT_PASSWORD')} | sudo -S"
# PRIVILEGES = ""

PROTOCOLS = [
    'HOPOPT',
    'ICMP',
    'IGMP',
    'GGP',
    'IPv4',
    'ST',
    'TCP',
    'CBT',
    'EGP',
    'IGP',
    'BBN-RCC-MON',
    'NVP-II',
    'PUP',
    'ARGUS',
    'EMCON',
    'XNET',
    'CHAOS',
    'UDP',
    'MUX',
    'DCN-MEAS',
    'HMP',
    'PRM',
    'XNS-IDP',
    'TRUNK-1',
    'TRUNK-2',
    'LEAF-1',
    'LEAF-2',
    'RDP',
    'IRTP',
    'ISO-TP4',
    'NETBLT',
    'MFE-NSP',
    'MERIT-INP',
    'DCCP',
    '3PC',
    'IDPR',
    'XTP',
    'DDP',
    'IDPR-CMTP',
    'TP++',
    'IL',
    'IPv6',
    'SDRP',
    'IPv6-Route',
    'IPv6-Frag',
    'IDRP',
    'RSVP',
    'GRE',
    'DSR',
    'BNA',
    'ESP',
    'AH',
    'I-NLSP',
    'SWIPE',
    'NARP',
    'MOBILE',
    'TLSP',
    'SKIP',
    'IPv6-ICMP',
    'IPv6-NoNxt',
    'IPv6-Opts',
    'CFTP',
    'SAT-EXPAK',
    'KRYPTOLAN',
    'RVD',
    'IPPC',
    'SAT-MON',
    'VISA',
    'IPCV',
    'CPNX',
    'CPHB',
    'WSN',
    'PVP',
    'BR-SAT-MON',
    'SUN-ND',
    'WB-MON',
    'WB-EXPAK',
    'ISO-IP',
    'VMTP',
    'SECURE-VMTP',
    'VINES',
    'TTP',
    'IPTM',
    'NSFNET-IGP',
    'DGP',
    'TCF',
    'EIGRP',
    'OSPFIGP',
    'Sprite-RPC',
    'LARP',
    'MTP',
    'AX.25',
    'IPIP',
    'MICP',
    'SCC-SP',
    'ETHERIP',
    'ENCAP',
    'GMTP',
    'IFMP',
    'PNNI',
    'PIM',
    'ARIS',
    'SCPS',
    'QNX',
    'A/N',
    'IPComp',
    'SNP',
    'Compaq-Peer',
    'IPX-in-IP',
    'VRRP',
    'PGM',
    'L2TP',
    'DDX',
    'IATP',
    'STP',
    'SRP',
    'UTI',
    'SMP',
    'SM',
    'PTP',
    'ISISoverIPv4',
    'FIRE',
    'CRTP',
    'CRUDP',
    'SSCOPMCE',
    'IPLT',
    'SPS',
    'PIPE',
    'SCTP',
    'FC',
    'RSVP-E2E-IGNORE',
    'MobilityHeader',
    'UDPLite',
    'MPLS-in-IP',
    'manet',
    'HIP',
    'Shim6',
    'WESP',
    'ROHC',
    'Ethernet',
    'AGGFRAG',
    'Reserved',
    'Unknown'
]


def handler(signum, frame):
    print ("Times up! Exiting...")
    exit(0)

# sudo ettercap -Tq --iface ens160 -w d24.pcap --mitm ARP /10.0.12.1// /10.0.12.35//
def mitm(iface="default", method="ARP", target1=None, target2=None, times=1):
    time_start = datetime.now().strftime("%Y%m%d%H%M%S%f")
    filename = f"{iface}_{time_start}_{times}"
    cmd = f"{PRIVILEGES} ettercap -Tq --mitm {method} -w $PWD/media/pcap/{filename}.pcap"
    if iface != "default":
        cmd += f" --iface {iface}"
    cmd += f" /{target1}//"
    cmd += f" /{target2}//"
    proc = subprocess.Popen([cmd], shell= True)
    time.sleep(times * 60)
    print(f"exit after {times} minutes")
    proc.terminate()
    os.system(f'{PRIVILEGES} chmod a+rwx $PWD/media/pcap/{filename}.pcap')
    os.system(f'{PRIVILEGES} tshark -r $PWD/media/pcap/{filename}.pcap -T fields -e frame.number -e eth.src -e eth.dst -e ip.src -e ip.dst -e frame.len -e ip.proto -e ipv6.nxt -e _ws.col.Info -E header=y -E separator=, -E occurrence=f> $PWD/media/csv/{filename}.csv')
    print(filename)
    
    # filename = "wlp7s0_1676084796.555298"
    
    file = open(f'./media/csv/{filename}.csv', 'r+')

    lines = file.readlines()
    res = []
    lines = lines[1:]
    for line in lines: 
        line = line.replace(', ', ';')
        row = line.split(',')
        proto_id = int((row[6] + row[7]) if (row[6] + row[7]) != '' else -1)
        te = {
        'id': int(row[0]), 
        'source': row[1], 
        'dest': row[2], 
        'ipSource': row[3] if row[3] != '' else "Broadcast", 
        'ipDest': row[4] if row[4] != '' else "Broadcast",
        'length': int(row[5]), 
        'protocol': PROTOCOLS[proto_id], 
        'info': row[8], 
        }
        res.append(te)
    
    return {
        'url': f'{filename}.pcap',
        'details': res
    }


def openVM(path=None):
    cmd = f"vmplayer {path}"
    proc = subprocess.Popen([cmd], shell=True)


if __name__ == "__main__":
    # mitm("ens160", "ARP", "10.0.12.1", "10.0.12.35", 2)
    openVM("/mnt/workspace/data/vm/win7pro/win7pro.vmx")