from datetime import datetime
import pandas as pd

def filename_gen(inf):
  return f"{inf}_{datetime.now().timestamp()}"


PROTOCOLS = [
    'HOPOPT',
    'ICMP',
    'IGMP',
    'GGP',
    'IPv4',
    'ST',
    'TCP',
    'CBT',
    'EGP',
    'IGP',
    'BBN-RCC-MON',
    'NVP-II',
    'PUP',
    'ARGUS',
    'EMCON',
    'XNET',
    'CHAOS',
    'UDP',
    'MUX',
    'DCN-MEAS',
    'HMP',
    'PRM',
    'XNS-IDP',
    'TRUNK-1',
    'TRUNK-2',
    'LEAF-1',
    'LEAF-2',
    'RDP',
    'IRTP',
    'ISO-TP4',
    'NETBLT',
    'MFE-NSP',
    'MERIT-INP',
    'DCCP',
    '3PC',
    'IDPR',
    'XTP',
    'DDP',
    'IDPR-CMTP',
    'TP++',
    'IL',
    'IPv6',
    'SDRP',
    'IPv6-Route',
    'IPv6-Frag',
    'IDRP',
    'RSVP',
    'GRE',
    'DSR',
    'BNA',
    'ESP',
    'AH',
    'I-NLSP',
    'SWIPE',
    'NARP',
    'MOBILE',
    'TLSP',
    'SKIP',
    'IPv6-ICMP',
    'IPv6-NoNxt',
    'IPv6-Opts',
    'CFTP',
    'SAT-EXPAK',
    'KRYPTOLAN',
    'RVD',
    'IPPC',
    'SAT-MON',
    'VISA',
    'IPCV',
    'CPNX',
    'CPHB',
    'WSN',
    'PVP',
    'BR-SAT-MON',
    'SUN-ND',
    'WB-MON',
    'WB-EXPAK',
    'ISO-IP',
    'VMTP',
    'SECURE-VMTP',
    'VINES',
    'TTP',
    'IPTM',
    'NSFNET-IGP',
    'DGP',
    'TCF',
    'EIGRP',
    'OSPFIGP',
    'Sprite-RPC',
    'LARP',
    'MTP',
    'AX.25',
    'IPIP',
    'MICP',
    'SCC-SP',
    'ETHERIP',
    'ENCAP',
    'GMTP',
    'IFMP',
    'PNNI',
    'PIM',
    'ARIS',
    'SCPS',
    'QNX',
    'A/N',
    'IPComp',
    'SNP',
    'Compaq-Peer',
    'IPX-in-IP',
    'VRRP',
    'PGM',
    'L2TP',
    'DDX',
    'IATP',
    'STP',
    'SRP',
    'UTI',
    'SMP',
    'SM',
    'PTP',
    'ISISoverIPv4',
    'FIRE',
    'CRTP',
    'CRUDP',
    'SSCOPMCE',
    'IPLT',
    'SPS',
    'PIPE',
    'SCTP',
    'FC',
    'RSVP-E2E-IGNORE',
    'MobilityHeader',
    'UDPLite',
    'MPLS-in-IP',
    'manet',
    'HIP',
    'Shim6',
    'WESP',
    'ROHC',
    'Ethernet',
    'AGGFRAG',
    'Reserved',
    'Unknown'
]

import os
from pathlib import Path
import environ

# Build paths inside the project like this: BASE_DIR / 'subdir'.

env = environ.Env(
    ROOT_PASSWORD=(str, '')
)

BASE_DIR = Path(__file__).resolve().parent.parent
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))


PRIVILEGES = f"echo {env('ROOT_PASSWORD')} | sudo -S "
# PRIVILEGES = ""

def LiveCap(infs, packet_count = 100, time_out = None, protocol_filters = None):
  filename = f'{filename_gen("_".join(infs.split(" ")))}'
  
  # cmd = f'{PRIVILEGES}tshark'
  # for inf in infs.split(" "): 
  #   cmd += f' -i {inf}'
  # if protocol_filters != None and protocol_filters != '':
  #   for protocol_filter in protocol_filters.split(" "): 
  #     cmd += f' -f {protocol_filter}'
  # if time_out != None:
  #   cmd += f' -a duration:{time_out}'
  # cmd += f' -c {packet_count} -F pcap -w $PWD/media/pcap/{filename}.pcap'
  
  # print(cmd)
  
  # os.system(cmd)

  # os.system(
  #     f'{PRIVILEGES}tshark -r $PWD/media/pcap/{filename}.pcap -T fields -e frame.number -e eth.src -e eth.dst -e ip.src -e ip.dst -e frame.len -e ip.proto -e ipv6.nxt -e _ws.col.Info -E header=y -E separator=, -E occurrence=f> $PWD/media/csv/{filename}.csv')
  # print(filename)
  
  '''This piece of code work for both server and client'''
  cmd = f'{PRIVILEGES}tshark'
  for inf in infs.split(" "):
    cmd += f' -i {inf}'
  if protocol_filters != None and protocol_filters != '':
    for protocol_filter in protocol_filters.split(" "):
      cmd += f' -f {protocol_filter}'
  if time_out != None:
    cmd += f' -a duration:{time_out}'
  cmd += f' -c {packet_count} -F pcap -w /tmp/{filename}.pcap'

  print(cmd)

  os.system(cmd)

  cmd_copy = f'{PRIVILEGES}mv -f /tmp/{filename}.pcap $PWD/media/pcap/'

  os.system(cmd_copy)

  os.system(f'{PRIVILEGES}chmod a+rwx $PWD/media/pcap/{filename}.pcap')

  os.system(
      f'{PRIVILEGES}tshark -r $PWD/media/pcap/{filename}.pcap -T fields -e frame.number -e eth.src -e eth.dst -e ip.src -e ip.dst -e frame.len -e ip.proto -e ipv6.nxt -e _ws.col.Info -E header=y -E separator=, -E occurrence=f> $PWD/media/csv/{filename}.csv')
  print(filename)
  
  # filename = "wlp7s0_1676084796.555298"
  
  file = open(f'./media/csv/{filename}.csv', 'r+')

  lines = file.readlines()
  res = []
  lines = lines[1:]
  for line in lines: 
    line = line.replace(', ', ';')
    row = line.split(',')
    proto_id = int((row[6] + row[7]) if (row[6] + row[7]) != '' else -1)
    te = {
      'id': int(row[0]), 
      'source': row[1], 
      'dest': row[2], 
      'ipSource': row[3] if row[3] != '' else "Broadcast", 
      'ipDest': row[4] if row[4] != '' else "Broadcast",
      'length': int(row[5]), 
      'protocol': PROTOCOLS[proto_id], 
      'info': row[8], 
    }
    res.append(te)

  return {
    'url': f'{filename}.pcap',
    'details': res
  }

# LiveCap('wlp7s0 ham0', packet_count=1000, protocol_filters='udp')
