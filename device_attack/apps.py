from django.apps import AppConfig


class DeviceAttackConfig(AppConfig):
    name = 'device_attack'
