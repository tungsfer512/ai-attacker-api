import sys, os, signal

HYDRA_BANNER = "HYDRA"

def usage():
    print()
    print('-----------------------------------------------------------------------------------------------------------')
    print()
    print(HYDRA_BANNER)
    print()
    print('USAGE: python[python3] hydra.py [OPTIONS] <ip> <services> <login-url-if-services-is-"http-post-form">')
    print()
    print('OPTIONS:')
    print('Flag\t\t\t\tMô tả\t\t\t\t\t\t\tGiá trị mặc định')
    print('-t <minutes>\t\t\tThời gian tấn công\t\t\t\t\t(mặc định: 1)')
    print('-s <port>\t\t\tCổng để rà quét mật khẩu\t\t\t\t(mặc định: cổng mặc định của dịch vụ)')
    print('-l <username>\t\t\tUsername dùng để rà quét')
    print('-L <path/to/username_file>\tFile chứa các username dùng để rà quét')
    print('-p <password>\t\t\tPassword dùng để rà quét')
    print('-P <path/to/password_file>\tFile chứa các password dùng để rà quét')
    print('-w, -W <seconds>\t\tThời gian chờ giữa các lần thử username/password')
    print('-v, -V\t\t\t\tHiển thị thông tin cho từng lần quét')
    print('-h, --help\t\t\tShows this help')
    print()
    print('-----------------------------------------------------------------------------------------------------------')

def error(msg):
    # print help information and exit:
    sys.stderr.write(str(msg+"\n"))
    usage()
    sys.exit(2)

def handler(signum, frame):
    print ("Times up! Exiting...")
    exit(0)

def main():
    signal.signal(signal.SIGALRM, handler)
    if len(sys.argv) < 2:
        error('Please supply at least the ip')
    if sys.argv[1] == '-h':
        usage()
        sys.exit()
        
    options = sys.argv[1:]
    print(options)
    times = 1
    pops = []
    for i in range(len(options)):
        if options[i] == "-t":
            times = int(options[i + 1])
            pops.append(i + 1)
            pops.append(i)
    login_url = ""
    print(pops)
    pops.sort(reverse=True)
    print(pops)
    for p in pops:
        options.pop(p)
    for i in range(len(options)):
        if options[i] == "http-post-form":
            login_url = options[i + 1]
            options.pop(i + 1)
            break
    options.append("-I")
    cmd = " ".join(options)
    print(cmd)
    cmd = "hydra " + cmd
    cmd += f' "{login_url}"'
    print(f"Quit after {times} minutes")
    signal.alarm(times * 60)
    i = 0
    while True:
        print("Rà quét mật khẩu lần thứ", (i + 1))
        os.system(cmd)
        i += 1

if __name__ == "__main__":
    main()
