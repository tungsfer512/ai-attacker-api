import sys, os, time, signal

NMAP_BANNER = "NMAP"

def usage():
    print()
    print('-----------------------------------------------------------------------------------------------------------')
    print()
    print(NMAP_BANNER)
    print()
    print('USAGE: python[python3] nmap.py <ip> [OPTIONS]')
    print()
    print('OPTIONS:')
    print('Flag\t\t\t\t\t\tMô tả\t\t\t\t\t\t\t\tGiá trị mặc định')
    print('-t <minutes>\t\t\t\t\tThời gian tấn công\t\t\t\t\t\t(mặc định: 1)')
    print('-sS\t\t\t\t\t\tTCP SYN scan')
    print('-sT\t\t\t\t\t\tTCP Connect scan')
    print('-sA\t\t\t\t\t\tTCP ACK scan')
    print('-sU\t\t\t\t\t\tUDP scan')
    print('-sN\t\t\t\t\t\tTCP Null scan')
    print('-sF\t\t\t\t\t\tTCP FIN scan')
    print('-sX\t\t\t\t\t\tTCP Xmas scan')
    print('-s0\t\t\t\t\t\tIP Protocol scan')
    print('-F\t\t\t\t\t\tFast scan')
    print('-6\t\t\t\t\t\tIP v6 scan')
    print('-O\t\t\t\t\t\tOS Detection')
    print('-d/-dd\t\t\t\t\t\tDebug level 1/2')
    print('-h, --help\t\t\t\t\tShows this help')
    print()
    print('-----------------------------------------------------------------------------------------------------------')

def error(msg):
    # print help information and exit:
    sys.stderr.write(str(msg+"\n"))
    usage()
    sys.exit(2)

def handler(signum, frame):
    print ("Times up! Exiting...")
    exit(0)


def main():
    signal.signal(signal.SIGALRM, handler)
    if len(sys.argv) < 2:
        error('Please supply at least the ip')
    if sys.argv[1] == '-h':
        usage()
        sys.exit()
        
    options = sys.argv[1:]
    print(options)
    times = 1
    pops = []
    for i in range(len(options)):
        if options[i] == "-t":
            times = int(options[i + 1])
            pops.append(i + 1)
            pops.append(i)
    print(pops)
    pops.sort(reverse=True)
    print(pops)
    for p in pops:
        options.pop(p)
    if "-Pn" not in options:
        options.append("-Pn")
    if "--min-rate" not in options:
        options.append("--min-rate")
        options.append("1000")
    cmd = " ".join(options)
    cmd = "echo bvt5122001 | sudo -S nmap " + cmd
    print(cmd)
    print(f"Quit after {times} minutes")
    signal.alarm(times * 60)
    i = 0
    while True:
        print("Rà quét cổng lần thứ", (i + 1))
        os.system(cmd)
        i += 1

if __name__ == "__main__":
    main()
