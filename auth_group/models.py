from django.db import models

# Create your models here.
class AuthGroup(models.Model):
    
    name = models.CharField(max_length=150)
    
    class Meta:
        managed = False
        db_table = 'auth_group'