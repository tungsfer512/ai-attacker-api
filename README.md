# Iot Attacker

## 1. Description

This project is a web application that provides a web interface for users to perform attacks on IoT devices. The project is built on Django framework and uses PostgreSQL as database.

## 2. Installation

### 2.1. Requirements

- CPU: Minimum is Intel Core i3 or similar. Recommended is Intel Core i5 or greater.
- Ram: Minimum is 4GB. Recommended is 8GB or greater.
- Storage: Minimum is 70GB. Recommended is 100GB or greater.
- Operating system: Ubuntu >= 18.04 or similar Linux distributions
- Python: Version >= 3.5 and <= 3.8 *```(recommend version 3.8)```*
- Git
- Virtual environment: venv, pipvenv, conda, miniconda,... (Optional)
- Database: PostgreSQL >= 12.0

### 2.2. Linux packages

Open terminal and install all packages in `./IotAttackerAPI/sysrequirements.txt` file.

#### 2.2.1. Fping

```bash
sudo apt update
sudo apt install -y fping
```

#### 2.2.2. Nmap

```bash
sudo apt-get update
sudo apt-get install nmap
```

#### 2.2.3. Hping3

```bash
sudo apt update
sudo apt install -y hping3 
```

#### 2.2.4. WireShark

```bash
sudo apt update
sudo apt install -y wireshark
```

*If you see this notification, select "Yes" and press "Enter"*
![Config informations](./assets/config_wireshark.webp "Config informations")

#### 2.2.5. TShark

```bash
sudo apt update
sudo apt install -y tshark
```

#### 2.2.6. Ettercap

```bash
sudo apt update
sudo apt install -y ettercap-text-only
sudo apt install -y ettercap-graphical
```

#### 2.2.7. Tcpxtract

```bash
sudo apt-get update
sudo apt-get install -y tcpxtract
```

#### 2.2.8. Iptables

```bash
sudo apt-get update
sudo apt-get install -y iptables
```

#### 2.2.9. Urlsnarf

```bash
sudo apt-get update
sudo apt-get install -y dsniff
```

#### 2.2.10. Hydra

```bash
sudo apt-get update
sudo apt-get install -y build-essential
sudo apt-get install -y hydra
```

#### 2.2.11. OpenSSL

```bash
sudo apt-get update
sudo apt-get install -y openssl
sudo apt-get install libssl-dev
```

### 2.3. Vitual environment (Optional)

You will install all packages in a virtual environment to avoid conflict with other projects.

- In this project, we use `anaconda` to create a virtual environment.
- Install `anaconda` by following this link [Install anaconda in Linux](https://docs.anaconda.com/anaconda/install/linux/).
- Create a virtual environment by running command ```conda create -n <env_name> python=<python_version>```. (Recommend python version is 3.8)
- Activate the virtual environment by running command ```conda activate <env_name>```.

### 2.4. Database

The project uses PostgreSQL as database. So, You need to install PostgreSQL or change settings to use other database.

```bash
sudo apt update
sudo apt install -y postgresql postgresql-contrib
sudo systemctl start postgresql.service
sudo -i -u postgres
psql
ALTER USER postgres PASSWORD '<your password>';
\q
exit
```

### 2.5. Install python packages

In the root folder of project, run command `pip install -r requirements.txt` to install all required packages.

## 3. Start

- Create a database for the project and change infomations in `.env` file.

    Example:

    ```.env
    SECRET_KEY=abc
    DEBUG=True
    ALLOWED_HOSTS=* localhost 127.0.0.1
    DB_ENGINE=django.db.backends.postgresql
    DB_NAME=iot-attacker
    DB_USER=postgres
    DB_PASSWORD=postgres
    DB_HOST=localhost
    DB_PORT=5432
    STATIC_ROOT=static
    MEDIA_ROOT=media
    ROOT_PASSWORD=Passwd2@12345
    ```

- Run command ```python manage.py makemigrations``` to apply all change in your code.
- Next, run command ```python manage.py migrate``` to create tables in database.
- Then, run command ```python manage.py loaddata initial_data.json``` to load data to database. Default username and password is `seclab` and `seclab@123`.
- Run command ```python manage.py runserver 0.0.0.0:$PORT``` to start the project (0.0.0.0 make your IP is public IP address, $PORT is the port number of the server).
