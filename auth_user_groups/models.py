from django.db import models
from auth_user.models import AuthUser
from auth_group.models import AuthGroup

# Create your models here.
class AuthUserGroups(models.Model):
    
    user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)
    group = models.ForeignKey(AuthGroup, on_delete=models.CASCADE)
    
    class Meta:
        managed = False
        db_table = 'auth_user_groups'